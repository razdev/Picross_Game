﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class picross_game : MonoBehaviour {

    [Header("nombres indicatif pour chaque picross")]
    public String[] line_numbers;       //les numéros indicatifs des lignes sous forme <nb_line1>,<nb_line2>,....,<nb_linen>
    public String[] column_numbers;

    [Header("current et final tableau de pixel")]
    public int[] square_int;
    public int[] square_int_result;

    [Header("user stats")]
    public float pourcentage_completion = 0.0f;

    [Header("other things")]
    public Texture cross;
    public bool ai_mode_activated;
    [Space(10)]
    public int picross_level;


    void Start () {
        picross_level = 1;
        square_int = new int[line_numbers.Length*column_numbers.Length];
        square_int_result = new int[line_numbers.Length * column_numbers.Length];

        reset_numbers_text();
        set_numbers_text();

        StartCoroutine(ai_completion());
    }



    void reset_numbers_text() { //Réinitialisation des lignes et colonnes d'indication de nombres dans les lignes/colonnes avant affectation
        for (int i = 0; i < line_numbers.Length; i++)
        {
            GameObject.Find("Line" + (i + 1) + "Numbers").GetComponent<Text>().text = "";
        }
        for (int j = 0; j < column_numbers.Length; j++)
        {
            GameObject.Find("Column" + (j + 1) + "Numbers").GetComponent<Text>().text = "";
        }
    }




    void set_numbers_text () { //affectation des nombres dans les lignes/colonnes en accord avec ce qui a été initialisé dans l'éditeur sous forme {<nb1>,...,<nbn>}
        for (int i = 0; i < line_numbers.Length; i++)
        {
            String[] current_line_numbers = line_numbers[i].Split(',');
            
            for (int j = 0; j < current_line_numbers.Length; j++)
            {
                GameObject.Find("Line" + (i + 1) + "Numbers").GetComponent<Text>().text += current_line_numbers[j].ToString();
                if (j != (current_line_numbers.Length - 1))
                    GameObject.Find("Line" + (i + 1) + "Numbers").GetComponent<Text>().text += " ";
            }
            
        }
        for (int i = 0; i < column_numbers.Length; i++)
        {
            String[] current_column_numbers = column_numbers[i].Split(',');
            for (int j = 0; j < current_column_numbers.Length; j++)
            {
                GameObject.Find("Column" + (i + 1) + "Numbers").GetComponent<Text>().text += current_column_numbers[j].ToString();
                if (j != (current_column_numbers.Length - 1))
                    GameObject.Find("Column" + (i + 1) + "Numbers").GetComponent<Text>().text += "\n";
            }
        }
    }


    int[] eligible (String[] line_numbers_indicators,int[] current_line_completion, int debug_mode) {

        int nb_noir = 0;
        int nb_noir_max = 0;
        for (int k = 0; k < current_line_completion.Length; k++)    //créé la ligne complète la plus sérrée à gauche possible
        {
            if (current_line_completion[k] == 1)
                nb_noir++;
        }
        for (int k = 0; k < line_numbers_indicators.Length; k++)    //créé la ligne complète la plus sérrée à gauche possible
        {
            nb_noir_max+=int.Parse(line_numbers_indicators[k]);
        }
        if(nb_noir==nb_noir_max)
        {
            for (int k = 0; k < current_line_completion.Length; k++)    //créé la ligne complète la plus sérrée à gauche possible
            {
                if (current_line_completion[k] != 1)
                    current_line_completion[k] = 2;
            }
            return current_line_completion;
        }
        int[] virtual_line_completion_left= new int[current_line_completion.Length];
        int[] virtual_line_completion_right = new int[current_line_completion.Length];

        int[] eligible_start = new int[line_numbers_indicators.Length];
        int[] eligible_end = new int[line_numbers_indicators.Length];

        int loop_index = 0;

        for (int k = 0; k < current_line_completion.Length; k++)    //créé la ligne complète la plus sérrée à gauche possible
        {
            if (current_line_completion[k] == 2)
            {
                virtual_line_completion_left[k] = 2;
            }
            
            else if (loop_index < line_numbers_indicators.Length)
            {
                
                bool can_complete_block = true;
                int i;
                for (i = k; i < k + int.Parse(line_numbers_indicators[loop_index]); i++)
                {
                    if (current_line_completion[i] == 2)
                        can_complete_block = false;

                }
                if (i != current_line_completion.Length && current_line_completion[i] == 1)
                    can_complete_block = false;

                if (can_complete_block)
                {
                    
                    for (i = k; i < k + int.Parse(line_numbers_indicators[loop_index]); i++)
                    {
                        virtual_line_completion_left[i] = 1;
                    }
                    if (i != current_line_completion.Length)
                        virtual_line_completion_left[i] = 2;

                    loop_index++;
                    if (loop_index == line_numbers_indicators.Length)
                    {
                        bool there_is_still_black_on_right = false;
                        for (int j = i; j < current_line_completion.Length; j++)
                        {
                            if (current_line_completion[j] == 1)
                            {
                                there_is_still_black_on_right = true;
                            }

                        }
                        if (there_is_still_black_on_right)
                        {
                            loop_index--;
                            for (i = k; i < k + int.Parse(line_numbers_indicators[loop_index]); i++)
                            {
                                virtual_line_completion_left[i] = 0;
                            }
                        }
                        else
                        {
                            for (int j = i; j < current_line_completion.Length; j++)
                            {
                                virtual_line_completion_left[j] = 2;
                            }
                        }
                    }
                    else
                        k = i;
                }
                else
                {
                    virtual_line_completion_left[k] = 2;
                }
            }
        }

        loop_index = (line_numbers_indicators.Length - 1);

        for (int k = (current_line_completion.Length - 1); k >= 0; k--)    //créé la ligne complète la plus sérrée à droite possible
        {
            if (current_line_completion[k] == 2)
            {
                virtual_line_completion_right[k] = 2;
            }
            else if (loop_index >=0)
            {
                
                bool can_complete_block = true;
                int i;
                for (i = k; i > k - int.Parse(line_numbers_indicators[loop_index]); i--)
                {
                    if (current_line_completion[i] == 2)
                        can_complete_block = false;

                }
                if (i != -1 && current_line_completion[i] == 1)
                    can_complete_block = false;

                if (can_complete_block)
                {

                    for (i = k; i > k - int.Parse(line_numbers_indicators[loop_index]); i--)
                    {
                        virtual_line_completion_right[i] = 1;
                    }
                    if (i != -1)
                        virtual_line_completion_right[i] = 2;

                    loop_index--;
                    if (loop_index == -1)
                    {
                        bool there_is_still_black_on_left = false;
                        for (int j = i; j >= 0; j--)
                        {
                            if (current_line_completion[j] == 1)
                                there_is_still_black_on_left = true;
                        }
                        if (there_is_still_black_on_left)
                        {
                            loop_index++;
                            for (i = k; i > k - int.Parse(line_numbers_indicators[loop_index]); i--)
                            {
                                virtual_line_completion_right[i] = 0;
                            }
                        }
                        else
                        {
                            for (int j = current_line_completion.Length; j <= i; j++)
                            {
                                virtual_line_completion_left[j] = 2;
                            }
                            k = i;
                        }
                    }
                    else
                        k = i;
                }
                else
                {
                    virtual_line_completion_right[k] = 2;
                }
            }
        }

        int eligible_index_left = 0;            //on va créer un tableau d'ordonnacement des blocs noirs dans l'ordre d'apparition
        int eligible_index_right = 0;           //à gauche et à droite
        bool found_cross = false;


        if (virtual_line_completion_left[0] == 1)
        {
            eligible_index_left++;
            eligible_start[0] = 0;
        }
        else
            found_cross = true;


        for (int k = 1; k < current_line_completion.Length; k++)
        {
            if (found_cross && virtual_line_completion_left[k] == 1)
            {
                found_cross = false;
                eligible_start[eligible_index_left] = k;
                eligible_index_left++;
            }
            if (virtual_line_completion_left[k] == 2)
            {
                found_cross = true;
            }
        }

        if(eligible_start[0]!=0)
        {
            for (int k = 0; k < eligible_start[0]; k++)
            {
                current_line_completion[k] = 2;
            }
        }


        found_cross = false;

        if (virtual_line_completion_right[0] == 1)
        {
            eligible_index_right++;
            eligible_end[0] = 0;
        }
        else
            found_cross = true;


        for (int k = 1; k < current_line_completion.Length; k++)
        {
            if (found_cross && virtual_line_completion_right[k] == 1)
            {
                found_cross = false;
                eligible_end[eligible_index_right] = k;
                eligible_index_right++;
            }
            if (virtual_line_completion_right[k] == 2)
            {
                found_cross = true;
            }
        }


        if (eligible_end[line_numbers_indicators.Length-1]+int.Parse(line_numbers_indicators[line_numbers_indicators.Length-1]) < current_line_completion.Length)
        {
            for (int k = eligible_end[line_numbers_indicators.Length - 1] + int.Parse(line_numbers_indicators[line_numbers_indicators.Length - 1]); k < current_line_completion.Length; k++)
            {
                current_line_completion[k] = 2;
            }
        }

        for (int k=0;k < line_numbers_indicators.Length;k++)
        {
            if((eligible_end[k]-eligible_start[k])< int.Parse(line_numbers_indicators[k]))
            {
                int i = eligible_end[k];
                if ((eligible_end[k] - eligible_start[k]) == 0 && i != 0)
                    current_line_completion[i-1] = 2;
                for (i= eligible_end[k];i< eligible_end[k]+(int.Parse(line_numbers_indicators[k])- (eligible_end[k] - eligible_start[k]));i++)
                {
                    current_line_completion[i] = 1;
                }
                if((eligible_end[k] - eligible_start[k])==0 && i!= current_line_completion.Length)
                    current_line_completion[i] = 2;

            }
        }
        return current_line_completion;
    }


    /*IEnumerator*/ void check_simple_lines() {
        
        for (int i = 0; i < line_numbers.Length; i++)                                      //check si on a une ligne simple (remplissage auto de toute la ligne)
        {
            //yield return new WaitForSeconds(0.2f);
            String[] current_line_numbers = line_numbers[i].Split(',');
            if (current_line_numbers.Length == 1)
            {
                if (int.Parse(current_line_numbers[0]) > (column_numbers.Length / 2))        //si on a un 4 dans une ligne de 5 par ex (rempli les 3 du centre)
                {
                    for (int k = (column_numbers.Length - int.Parse(current_line_numbers[0])); k < column_numbers.Length - (column_numbers.Length - int.Parse(current_line_numbers[0])); k++)
                    {
                        square_int_result[i * column_numbers.Length + k] = 1;
                    }
                }
            }
            else
            {
                for (int j = 0; j < current_line_numbers.Length; j++)
                {
                    int count_to_j = 0;
                    int space_available = 0;
                    bool went_through = false;
                    for (int k = 0; k < current_line_numbers.Length; k++)
                    {
                        if (k != j)
                        {
                            space_available += (1 + int.Parse(current_line_numbers[k]));
                            if (!went_through)
                                count_to_j += (1 + int.Parse(current_line_numbers[k]));
                        }
                        else
                            went_through = true;
                    }

                    space_available = (column_numbers.Length - space_available);

                    if (int.Parse(current_line_numbers[j]) > (space_available / 2))
                    {
                        for (int k = count_to_j + (space_available - int.Parse(current_line_numbers[j])); k < count_to_j + space_available - (space_available - int.Parse(current_line_numbers[j])); k++)
                        {
                            square_int_result[i * column_numbers.Length + k] = 1;
                        }
                    }
                }
            }
        }
        //StartCoroutine(check_simple_columns());
    }


    /*IEnumerator*/ void check_simple_columns () {
        for (int i = 0; i < column_numbers.Length; i++)                                      //check si on a une colonne simple (remplissage auto de toute la colonne)
        {
            //yield return new WaitForSeconds(0.2f);
            String[] current_column_numbers = column_numbers[i].Split(',');
            if (current_column_numbers.Length == 1)
            {
                if (int.Parse(current_column_numbers[0]) > (line_numbers.Length / 2))        //si on a un 4 dans une colonne de 5 par ex (rempli les 3 du centre)
                {
                    for (int k = (line_numbers.Length - int.Parse(current_column_numbers[0])); k < line_numbers.Length - (line_numbers.Length - int.Parse(current_column_numbers[0])); k++)
                    {
                        square_int_result[k * column_numbers.Length + i] = 1;
                    }
                }
            }
            else
            {
                for (int j = 0; j < current_column_numbers.Length; j++)
                {
                    int count_to_j = 0;
                    int space_available = 0;
                    bool went_through = false;
                    for (int k = 0; k < current_column_numbers.Length; k++)
                    {
                        if (k != j)
                        {
                            space_available += (1 + int.Parse(current_column_numbers[k]));
                            if (!went_through)
                                count_to_j += (1 + int.Parse(current_column_numbers[k]));
                        }
                        else
                            went_through = true;
                    }

                    space_available = (line_numbers.Length - space_available);

                    if (int.Parse(current_column_numbers[j]) > (space_available / 2))
                    {
                        for (int k = count_to_j + (space_available - int.Parse(current_column_numbers[j])); k < count_to_j + space_available - (space_available - int.Parse(current_column_numbers[j])); k++)
                        {
                            square_int_result[k * column_numbers.Length + i] = 1;
                        }
                    }
                }
            }
        }
        //StartCoroutine(recursive_completion());
    }

    IEnumerator recursive_completion () {
        for(int a=0;a<=1000;a++)
        {
            if (a == 0 || a == 5)
                picross_level++;
            check_completion();
            float before_pourc_complt = pourcentage_completion;
            yield return new WaitForSeconds(1.0f);
            for (int i = 0; i < line_numbers.Length; i++)                                      //check si on a une ligne simple (remplissage auto de toute la ligne)
            {
                String[] current_line_numbers = line_numbers[i].Split(',');
                int[] current_line_square= new int[column_numbers.Length];

                for (int k = 0; k < column_numbers.Length; k++)
                {
                    current_line_square[k] = square_int_result[i*column_numbers.Length+k];
                }
                current_line_square = eligible(current_line_numbers, current_line_square,0);
                for (int k = 0; k < column_numbers.Length; k++)
                {
                    square_int_result[i * column_numbers.Length + k]=current_line_square[k];
                }
            }
            for (int i = 0; i < column_numbers.Length; i++)                                      //check si on a une ligne simple (remplissage auto de toute la ligne)
            {
                String[] current_column_numbers = column_numbers[i].Split(',');
                int[] current_column_square = new int[line_numbers.Length];

                for (int k = 0; k < line_numbers.Length; k++)
                {
                    current_column_square[k] = square_int_result[i+ k* column_numbers.Length];
                }
                if(i==5)
                    current_column_square = eligible(current_column_numbers, current_column_square,1);
                else
                    current_column_square = eligible(current_column_numbers, current_column_square, 0);
                for (int k = 0; k < line_numbers.Length; k++)
                {
                    square_int_result[i + k * column_numbers.Length] = current_column_square[k];
                }
            }
            check_completion();
            float after_pourc_complt = pourcentage_completion;
            if(before_pourc_complt==after_pourc_complt)
            {
                Debug.Log("bot as finished");
                if (pourcentage_completion != 0)
                    Debug.Log("bot couldn't complete the picross");
                else
                    Debug.Log("bot successfully completed the picross");
                picross_level = 3;
                break;
            }
        }


    }

    IEnumerator ai_completion () { //une ai qui complète le picross pour initialiser le résultat, si elle n'y arrive pas, le picross est infaisable
        yield return new WaitForSeconds(2.0f);
        //StartCoroutine(check_simple_lines());
        check_simple_lines();
        check_simple_columns();
        check_completion();
        if(pourcentage_completion!=0)
            StartCoroutine(recursive_completion());
    }



    void get_squares_datas () {
        for(int i=1; i<= line_numbers.Length; i++)
        {
            for (int j = 1; j <= column_numbers.Length; j++)
            {
                square_int[(i-1)* column_numbers.Length + (j-1)]=
                    GameObject.Find("Square" + i + "_" + j).GetComponent<picross_square>().square_step;
                if(ai_mode_activated)
                {
                    
                    if (square_int_result[(i - 1) * column_numbers.Length + (j - 1)] == 1)
                    {
                        GameObject.Find("Square" + i + "_" + j).GetComponent<RawImage>().texture = null;
                        GameObject.Find("Square" + i + "_" + j).GetComponent<RawImage>().color = new Color32(0, 0, 0, 255);
                    }
                    else if (square_int_result[(i - 1) * column_numbers.Length + (j - 1)] == 2)
                    {
                        GameObject.Find("Square" + i + "_" + j).GetComponent<RawImage>().color = new Color32(255, 255, 255, 255);
                        GameObject.Find("Square" + i + "_" + j).GetComponent<RawImage>().texture = cross;
                    }
                    else
                    {
                        GameObject.Find("Square" + i + "_" + j).GetComponent<RawImage>().texture = null;
                        GameObject.Find("Square" + i + "_" + j).GetComponent<RawImage>().color = new Color32(255, 255, 255, 255);
                    }
                }
                else
                {
                    if (square_int[(i - 1) * line_numbers.Length + (j - 1)] == 1)
                    {
                        GameObject.Find("Square" + i + "_" + j).GetComponent<RawImage>().texture = null;
                        GameObject.Find("Square" + i + "_" + j).GetComponent<RawImage>().color = new Color32(0, 0, 0, 255);
                    }
                    else if (square_int[(i - 1) * line_numbers.Length + (j - 1)] == 2)
                    {
                        GameObject.Find("Square" + i + "_" + j).GetComponent<RawImage>().color = new Color32(255, 255, 255, 255);
                        GameObject.Find("Square" + i + "_" + j).GetComponent<RawImage>().texture = cross;
                    }
                    else
                    {
                        GameObject.Find("Square" + i + "_" + j).GetComponent<RawImage>().texture = null;
                        GameObject.Find("Square" + i + "_" + j).GetComponent<RawImage>().color = new Color32(255, 255, 255, 255);
                    }
                }
            }
        }
    }


    void check_completion () {
        int correct_squares = 0;
        for (int i = 1; i <= line_numbers.Length; i++)
        {
            for (int j = 1; j <= column_numbers.Length; j++)
            {
                if (square_int[(i - 1) * column_numbers.Length + (j - 1)] == square_int_result[(i - 1) * column_numbers.Length + (j - 1)])
                    correct_squares++;
            }
        }
        pourcentage_completion=((100f * correct_squares) / (line_numbers.Length * column_numbers.Length));
    }


	void Update () {
        get_squares_datas();
        check_completion();
	}
}
